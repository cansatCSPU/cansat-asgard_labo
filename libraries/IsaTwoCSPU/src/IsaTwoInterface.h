/*
 * IsaTwoInterface.h
 * This file contains definitions shared by the CanSat, and the ground systems
 */

#pragma once 

/**  @ingroup IsaTwoCSPU
 *  Constants used to manage the type of CSV strings exchanged between the CanSat and 
 *  the ground systems.
 */
 enum class IsaTwoRecordType { 
	 	 	 	 // DO NOT USE VALUE 0 (value returned when string to int conversion fails).
                DataRecord=10,                /**< Value denoting a measurement record */
                CmdRequest=1,                /**< Value denoting a command (request)  */
                CmdResponse=2,               /**< Value denoting a command response   */
                StatusMsg=3,                 /**< Value denoting an unsollicitated status message from the can
                                                  Format of the record, after the RecordType field: 
                                                        - timestamp
                                                        - status message (string until end of record) */
				StringPart=4,				 /**< Value denoting a part of a string. The receiver should
												  not include any end-of-line between string parts */
                GroundDataRecord=100         /**< Value denoting a ground record */
 }; 

 /**  @ingroup IsaTwoCSPU
  *   Constants used to identify command requests. The possible parameters for each request are
  *   documented with each value. */
 enum class IsaTwoCmdRequestType {
	 	 	 	// DO NOT USE VALUE 0 (value returned when string to int conversion fails).
                InitiateCmdMode=10,  /**< Request switch to command mode. Parameters: none. */
                TerminateCmdMode=11, /**< Request switch to acquisition mode. Parameters: none.*/
                DigitalWrite=13,     /**< Request a write on digital pin. Parameters: pinNumber, state (0=LOW, 1=HIGH) */
                ListFiles=14,        /**< Request a list of files present on the SD Card
                                         Parameters: start seq. number (start list from n-th file. First file is 1.
                                                     number files to list (0= list all files) */
                GetFile=15,          /**< Request the transfer of a particular file
                                         Parameters: file name (mandatory), start byte (optional, default to 0),
                                         	 	 	 numBytes (optional, defaults to 10000). */
				GetCampaignStatus=16,/**< Request the status of the measurement campaign (started or not). */
				StartBuzzer=17,		/**< Request the operation of the buzzer to locate the can.
				 	 	 	 	 	 	 Parameter: duration in seconds */
				StartCampaign=18,	/**< Request the immediate start of the measurement campaign */
				StopCampaign=19		/**< Request the immediate stop of the measurement campaign */
 };

 /**  @ingroup IsaTwoCSPU
  *   Constants used to identify command responses.
  *   Every response can include an additional parameters which is a free, human-readable message.  */
 enum class IsaTwoCmdResponseType {
	 	 	 	 // DO NOT USE VALUE 0 (value returned when string to int conversion fails).
                CmdModeInitiated=10,          /**< Response to successful InitiateCmdMode. Parameters: none. */
                CmdModeTerminated=11,         /**< Response to successful TerminateCmdMode. Parameters: none. */
                DigitalWriteOK=12,            /**< Response to a successful DigitalWrite. Parameters: pinNumber, state (0=LOW, 1=HIGH).  */
                FileListEntry=13,             /**< Response to a ListFiles command. This message is repeated for each file.
                 	 	 	 	 	 	 	 	   Parameters: file sequence number (from 1), file name, file size (0 if directory) */
                FileListComplete=14,          /**< Conclusion of a successful ListFiles command, sent after the last FileListEntry response to denote the end of list of file names.
                                                  Parameters: path = the listed path, the number of files listed. */
				CampaignStatus=15,	 		 /**< The current status of the campaign.
										          Parameters: status 1=started, 0=not started */
                FileContent=16,	             /**< Announces the transmission of the content of a file, surrounded by content markers (NumFileContentMarkers characters of type FileContentMarkerChar).
                						          Parameters: path = the transmitted file's path */
				InvalidPath=17,			 	 /**<  Response to ListFile or GetFile command, when provided path is invalid (inexistent,
												  inaccessible, not a directory when a file is expected, or not a file when a directory is expected...). */
                MissingCmdParameters=18,	 /**< Response to request missing mandatory parameters */
				MalformedRequest=19,		 /**< Response to a malformed request (no request type, request type is not a number...) */
				FileError=20,				 /**< Response to a file operation request failing for a generic file issue */
				UnsupportedRequestType=99	 /**< Request type was not recognized.
												  Parameters: the request type */
 };

 /** The content of a particular text file is transmitted surrounded by markers composed of
  *  NumFileContentMarkers characters of value StartFileContentMarker.
  */
 constexpr char StartFileContentMarker = '*';
 constexpr uint8_t NumFileContentMarkers  = 10;
