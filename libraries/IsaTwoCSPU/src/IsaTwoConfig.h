/*
   System-wide configuration for the  IsaTwo Project
*/

#pragma once

#include "Arduino.h"
#include "ArduCAM.h"
#include "limits.h"

// ********************************************************************
// ***************** Calibration settings to update frequently ********
// ********************************************************************

constexpr float SeaLevelPressure_HPa = 1021;  // Sea level pressure value to calculate actual altitude.

constexpr uint8_t IsaTwoLightMode = Auto; // or Auto, Sunny, Cloudy, Office, Home
constexpr uint8_t IsaTwoColorSaturation = Saturation0; // 4 -> -4 
constexpr uint8_t IsaTwoBrightness = Brightness_2; //4 -> -4
constexpr uint8_t IsaTwoContrast = Contrast0; //4 -> -4 

// ********************************************************************
// **************************** DEBUG SETTINGS ************************
// ********************************************************************

#define IGNORE_EEPROMS             // When defined, detected EEPROM chips are ignored, in order to avoid wasting write cycles during tests
// UNDEFINE THIS SYMBOL FOR OPERATIONS (when EEPROMs are used).
#define NO_EEPROMS_ON_BOARD		   // Define to turn off all EEPROM-related features.

#define PRINT_DIAGNOSTIC_AT_INIT   //Comment out to avoid startup diagnostic if memory usage must be reduced.
#define INIT_SERIAL
									// Comment definition out to reduce program size if there is no use
									// for the serial port at all (no debugging, no diagnostic at startup, etc.).
// AcquisitionProcess
//#define PRINT_ACQUIRED_DATA       // Define to have data printed in very readable format (DEBUGGING ONLY)
//#define PRINT_ACQUIRED_DATA_CSV     // Define to have data printed in CSV (1 line/record) format (DEBUGGING ONLY)

#define DEBUG_CSPU					// Comment out to avoid including any debugging code. DEFINE FOR OPERATION
//define USE_ASSERTIONS           // UNDEFINE THIS SYMBOL FOR OPERATIONS.
//define USE_TIMER                // Comment out to avoid timing output and overhead. UNDEFINE THIS SYMBOL FOR OPERATIONS
#include "DebugCSPU.h"             // include AFTER defining/undefining DEBUG, USE_ASSERTION and USE_TIMER.

// Enable/disable the various parts of the debugging here:
// 1. This group of debug settings must be set to 0 for operation
#define DBG_SETUP 1
#define DBG_INIT  0
#define DBG_LOOP  0
#define DBG_LOOP_MSG  1
#define DBG_ACQUIRE 0
#define DBG_STORAGE 0
#define DBG_SLOW_DOWN_TRANSMISSION 0   // Add a 500 msec delay in the RF transmission
#define DBG_CHECK_SPI_DEVICES 0	  // IsaTwo_HW_Scanner: print detection of SPI device.
#define DBG_SLAVE_SYNC 1		  // Document every resynchronisation of slave clock.

// 2. This group of debug settings should preferably be set to 1 for operation,
//    unless there is a memory shortage.
#define DBG_DIAGNOSTIC 1		  // Print messages in case of error only.
#define DBG_CAMPAIGN_STARTED 1

// ********************************************************************
// ************************ Mission parameters ************************
// ********************************************************************

// 1. For the master controller.
constexpr unsigned int IsaTwoAcquisitionPeriod = 70;  // Acquisition period in msec. If RF_TransmissionDelayEveryTwoNumbers>0,
                                                  	  // check carefully the actual duration of the acquisition+storage+transmission.
													  // Ditto for PRINT_ACQUIRED_DATA or any time-consuming feature.
constexpr byte imuSamplesPerReading=1;				  // Number of samples to average at each IMU reading.
												      // Keep to 1, increasing seems to degrade the result.
constexpr unsigned int IsaTwoCampainDuration = 300;   // Expected duration of the measurement campaign in seconds.
constexpr unsigned int IsaTwoSensorsWarmupDelay = 1000; // Delay in msec to allow for the sensors to be ready.

// Detection of take-off ("start campaign condition")
constexpr float AltitudeThresholdToStartCampaign=136.0f;
													  // The altitude above which the campaign is considered as
													  // started, whatever the speed conditions.
constexpr float MinSpeedToStartCompaign = 3.0f ;       // Minimum speed (m/s) to detect during NumSamplesToStartCampaign consecutive
													   // samples. Warning: MinSpeedToStartCompaign may not be > 10 m/s.
constexpr byte NumSamplesToAverageForStartingCampaign = (1000 / IsaTwoAcquisitionPeriod);
// Number of samples used to average the vertical velocity to detect the take off.
// WARNING: sensor accuracy is +- 1 m/s, so noise cause detection of
// apparent speed of 1/acquisitionPeriod m/S. For
// acquisition period = 200 msec, this can be 5 m/s ! It is therefore
// essential to have NumSamplesToStartCamapaign cover at least 1 second.


constexpr unsigned int SD_RequiredFreeMegs = 0;     // The number of free Mbytes required on the main SD card at startup (0=do not check).
constexpr uint16_t EEPROM_KeyValue = 0x1217;          // The key identifying a valid EEPROM header.

constexpr byte numDecimalPositionsToUse = 5; // The number of decimal positions to use in string representations of acquired float data.
constexpr byte maxUplinkMsgLenght=100;		 // The maximum number of characters in an uplink message (cmd to the RT-Commander).
constexpr unsigned long RT_CommanderTimeoutInMsec=15000L; // The inactivity duration after which the can resumes the acquisition mode
constexpr unsigned long ReportingPeriodWhileWaitingForStartCampaign=400; // msec. Period for reporting average speed and other
													// parameters while waiting for take-off.

#define RF_ACTIVATE_API_MODE		// Define to perform all RF communication in API mode.
#define RF_XBEE_MODULES_SET	'A'		// Define for the XBee pair in used.
									// Valid values: 'A', 'B', or 'T' (=TEST)
#ifdef RF_ACTIVATE_API_MODE
constexpr uint8_t RF_LongestStringLength=200;
#  if (RF_XBEE_MODULES_SET=='A')
	constexpr uint32_t CanXBeeAddressSH=0x0013a200;  // Address of can XBee module (API mode)
	constexpr uint32_t CanXBeeAddressSL=0x418FC759;
	constexpr uint32_t GroundXBeeAddressSH=0x0013a200;  // Address of RF-Transceiver XBee module (API mode)
	constexpr uint32_t GroundXBeeAddressSL=0x415E655F;
#  elif (RF_XBEE_MODULES_SET=='B')
	constexpr uint32_t CanXBeeAddressSH=0x0013a200;  // Address of can XBee module (API mode)
	constexpr uint32_t CanXBeeAddressSL=0x418FBBEB;
	constexpr uint32_t GroundXBeeAddressSH=0x0013a200;  // Address of RF-Transceiver XBee module (API mode)
	constexpr uint32_t GroundXBeeAddressSL=0x418FC78B;
#  elif (RF_XBEE_MODULES_SET=='T')					// Test modules
	constexpr uint32_t CanXBeeAddressSH=0x0013a200;  // Address of can XBee module (API mode)
	constexpr uint32_t CanXBeeAddressSL=0x41827f67;  // The one without antenna
	constexpr uint32_t GroundXBeeAddressSH=0x0013a200;  // Address of RF-Transceiver XBee module
	constexpr uint32_t GroundXBeeAddressSL=0x418fb90a;  // (the one with antenna)
#  else
#    error "Unknown XBee set in IsaTowConfig.h"
#  endif // RF_USE_FINAL_XBEE_MODULES
#endif // RF_ACTIVATE_API_MODE

// 2. Imager controller.
constexpr unsigned int Imager_DelayAfterEachImage = 0; // msec. Not required.
constexpr unsigned long Imager_LoopDelay = 0; // msec, delay in the imager's main loop.
constexpr unsigned long Imager_DelayAfterRegulatorOn = 50; // msec. delay after switching regulator on.
constexpr uint8_t Imager_LargeImageSize=OV2640_1600x1200;
constexpr uint8_t Imager_SmallImageSize=OV2640_160x120;

// ************************************************************************
// **** Sensors Stable settings (see calibration info on top of file) *****
// ************************************************************************

#define NXP_A 'A'							// IMU ids (use characters for readability
#define NXP_B 'B'
#define LSM_A 'a'
#define LSM_B 'b'
#define LSM_C 'c'							// The operational one.
// WARNING : complete below if adding new values !

#define IMU_REFERENCE	LSM_C 				// Valid values: one of the defines above

#define AHRS_FUSION_MADGWICK 'M'
#define AHRS_FUSION_MAHONY	 'm'
#define AHRS_FUSION_ALGORITHM AHRS_FUSION_MADGWICK // Valid values: one of the defines above

// ********************************************************************
// ********************* Hardware configuration  **********************
// ********************************************************************

#undef  ISATWO_USE_EEPROMS					// Undefine if the can does not include EEPROMS.

// Serial ports
constexpr  long USB_SerialBaudRate = 115200;              // baudrate on the serial interface of the µC board. Faster is best.
constexpr  long RF_SerialBaudRate  = 115200;              // baudrate to communicate with the RF board (must be consistent with XBee settings.
constexpr  byte GPS_SerialPortNumber = 1;
constexpr  byte RF_SerialPortNumber  = 2;

// I2C Slaves
// Two possible IMU boards:
constexpr byte I2C_LSM9DS0_Address = 0x1D; 		// LSM9DS0 Accel+Magnetometer (gyro on 0x6B)
constexpr byte I2C_LSM9DS0_SecondaryAddress = 0x6B;
constexpr byte I2C_PrecisionNXP_Address = 0x1F; // Precision NXP Accel+Magnetometer (gyro on 0x21)
constexpr byte I2C_PrecisionNXP_SecondaryAddress = 0x21;

// Actual addresses used.
#if (IMU_REFERENCE == NXP_A) || (IMU_REFERENCE == NXP_B)
#  define USE_NXP_PRECISION_IMU		// NXP Precision IMU is used
#elif (IMU_REFERENCE == LSM_A) || (IMU_REFERENCE == LSM_B) || (IMU_REFERENCE == LSM_C)
#  undef USE_NXP_PRECISION_IMU		// LSM9DS0 IMU is used
#else
#  error "Unexpected IMU_REFERENCE value in IsaTwoConfig.h"
#endif

constexpr byte I2C_lowestAddress = 0x10;
constexpr byte I2C_highestAddress = 0x7F;   // EEPROMS are at 0x50-0x57.
constexpr byte I2C_BMP_SensorAddress = 0x77;
constexpr byte I2C_CO2_SensorAddress = 0x5A;
#ifdef USE_NXP_PRECISION_IMU
	constexpr byte I2C_IMU_SensorAddress = I2C_PrecisionNXP_Address;
	constexpr byte I2C_IMU_SensorSecondaryAddress = I2C_PrecisionNXP_SecondaryAddress;
#else
	constexpr byte I2C_IMU_SensorAddress = I2C_LSM9DS0_Address;
	constexpr byte I2C_IMU_SensorSecondaryAddress = I2C_LSM9DS0_SecondaryAddress;
#endif
constexpr byte I2C_Camera_SensorAddress = 0x30;

constexpr byte unusedAnalogInPinNumber = 0; // An unused analog pin. Value read is used as random seed.

// Analog & digital pins allocation on MASTER
constexpr byte Thermistor_AnalogInPinNbr = A0 ;     // Analog input for thermistor
constexpr byte CO2_AnalogWakePinNbr=A5  ;             // Analog wake pin for CO2 sensor.
constexpr byte ImagerCtrl_MasterDigitalPinNbr = 6;  // The pin used by the master to control the imager (active LOW).
constexpr byte DbgCtrl_MasterDigitalPinNbr=12;		// The pin to pull down to have the master board wait for USB serial initialisation

// Analog & digital pins allocation on SLAVE board (Imager)
constexpr byte ImagerCtrl_SlaveDigitalPinNbr = 10;  // The pin used by the imager to get master control (active LOW).
constexpr byte DbgCtrl_ImagerDigitalPinNbr=9;		// The pin to pull down to have the imager board wait for USB serial initialisation
constexpr byte ImagingLED_DigitalPinNbr=6;			// The pin connected to LED to turn on when imaging.
constexpr uint32_t ImagerCamera_SPI_Speed=8000000;	// Imager Camera announces 8MHz SPI but this performance is not always achieved.
													// Requires very short wires, appropriate pull-up/down, and even so...

#if defined ARDUINO_TMinus
	// This is the operational configuration
	constexpr byte pin_HeartbeatLED = LED_BUILTIN;
	constexpr byte pin_InitializationLED = LED_BUILTIN1;
	constexpr byte pin_AcquisitionLED = LED_BUILTIN2;
	constexpr byte pin_StorageLED = LED_BUILTIN3;
	constexpr byte pin_TransmissionLED = LED_BUILTIN4;
	constexpr byte pin_CampaignLED = LED_BUILTIN5;
	constexpr byte pin_UsingEEPROM_LED = LED_BUILTIN6;
	// Master µC SPI Slaves
	constexpr byte MasterSD_CardChipSelect = 27; // Master µC SD-card (not actually on board with T-Minus)
	// Slave µC SPI slaves.
    constexpr byte ImagerSD_CardChipSelect = 12; 	// Slave µC SD-card.
	constexpr byte ImagerCamera_ChipSelect=5;		// Imager Camera CS.

#elif (defined ARDUINO_AVR_UNO || defined ARDUINO_AVR_MICRO)
	// This is an alternate configuration for development using a standard UNO board.
	constexpr byte pin_HeartbeatLED = LED_BUILTIN;
	constexpr byte pin_InitializationLED = 8;
	constexpr byte pin_AcquisitionLED = 0;
	constexpr byte pin_StorageLED = 0;
	constexpr byte pin_TransmissionLED = 0;
	constexpr byte pin_CampaignLED = 8;
	constexpr byte pin_UsingEEPROM_LED = 0;
    constexpr int defaultDAC_StepNumber = 1023;
    constexpr float referenceVoltage = 5;
    // Master µC SPI Slaves
    constexpr byte MasterSD_CardChipSelect = 10;  // Master µC SD-card (not actually on board with Uno)
	// Slave µC SPI slaves.
    constexpr byte ImagerSD_CardChipSelect = 12; 	// Slave µC SD-card.
	constexpr byte ImagerCamera_ChipSelect=5;		// Imager Camera CS.

#elif defined ARDUINO_SAMD_FEATHER_M0_EXPRESS
	// This is the operational target hardware.
	constexpr byte pin_HeartbeatLED = LED_BUILTIN;
	constexpr byte pin_InitializationLED = 0; // we don't have a free led for that.
	constexpr byte pin_AcquisitionLED = 0;	  // Our cycle is too short to use all LEDs.
	constexpr byte pin_StorageLED = 0;
	constexpr byte pin_TransmissionLED = 0;
	constexpr byte pin_CampaignLED = 8;		  // Built-in green led on Adalogger
	constexpr byte pin_UsingEEPROM_LED = 0;
	constexpr int defaultDAC_StepNumber = 4095;
	constexpr float referenceVoltage = 3.3;
	// Master µC SPI Slaves
	constexpr byte MasterSD_CardChipSelect = 4;   // On-board SD-card on Adalogger
	// Slave µC SPI slaves.
    constexpr byte ImagerSD_CardChipSelect = 12; 	// Slave µC SD-card.
	constexpr byte ImagerCamera_ChipSelect = 5;		// Imager Camera CS.
	constexpr byte ImagerRegulatorEnablePin = 11;    // Regulator cmd line (active HIGH).
#else
#error "Unexpected board. Please use either TMinus, Feather M0 Express or Uno, or complete the configuration file "
#endif
