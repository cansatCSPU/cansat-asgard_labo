#define DEBUG_CSPU
//#define USE_ASSERTIONS
//#define USE_TIMER
#include "DebugCSPU.h"
#include "Timer.h"
#include "EEPROM_BankWriter.h"
#define DBG_READ_WRITE 0
#define DBG_STORE_TO_EEPROM 0
#define DBG_GET_TOTAL_SIZE 0
#define DBG_DIAGNOSTIC 1
#define DBG_INIT_EEPROM_BANK 0
#define DBG_STORE_TO_EEPROM_PRINT_HEADER 0
#include "ExtEEPROM.h"

// Constructor
EEPROM_BankWriter::EEPROM_BankWriter(const byte theMaintenancePeriodInSec)  {
  hardware = NULL;
  flags.initialized = false;
  flags.headerToBeUpdated = false;
  flags.memoryFull = false;
  maintenancePeriodInSec = theMaintenancePeriodInSec;
}

// Initialize the EEPROM_Bank. Looks for an existing header in the EEPROM, creates one if none found.
// If any found, it checks the consistency in order to keep filling the memory.
bool EEPROM_BankWriter::init(const EEPROM_Key key, const HardwareScanner &hw, const byte recordSize) {
  if (hw.getNumExternalEEPROM() == 0) {
    DPRINTS(DBG_INIT_EEPROM_BANK, "No EEPROM available.")
    return false;
  }
  hardware = &hw;
  byte numBytes = readFromEEPROM(0, 0, (byte*)&header, sizeof header);
  if (numBytes != sizeof header) {
    DPRINTSLN(DBG_INIT_EEPROM_BANK, "Could not read header");
    return false;
  }
  if (header.headerKey != key)
  {
    DPRINTS(DBG_INIT_EEPROM_BANK, "No header found. Creating a new one.");
    header.headerKey = key;
    header.firstFreeByte = sizeof header;
    header.numChips = hw.getNumExternalEEPROM();
    header.chipLastAddress = hw.getExternalEEPROM_LastAddress(0);
    header.firstFreeChip = 0;
    header.recordSize = recordSize;
    numBytes = writeToEEPROM(0, 0, (byte*) &header, sizeof header);
    if (numBytes != sizeof header) {
      DPRINTSLN(DBG_INIT_EEPROM_BANK, "Could not write header");
      return false;
    }
  }
  else
  {
    DPRINTSLN(DBG_INIT_EEPROM_BANK, "Valid header found");
    // Header found: let's check it is consistent.
    if (header.numChips != hw.getNumExternalEEPROM()) {
      DPRINTSLN(DBG_DIAGNOSTIC, "** Inconsistency between numChips and hw results");
      return false;
    }
    if (header.chipLastAddress != hw.getExternalEEPROM_LastAddress(0)) {
      DPRINTSLN(DBG_DIAGNOSTIC, "** Inconsistency between size of chips and hw results");
      return false;
    }
    if (header.recordSize != recordSize) {
      DPRINTSLN(DBG_DIAGNOSTIC, "** Inconsistency between recordSize in the header and recordSize");
      return false;
    }
    //
    // Ok, header seems to be ok....
  }
  flags.initialized = true;
  return true;
}

unsigned long EEPROM_BankWriter::getTotalSize() const {
  DASSERT(flags.initialized);
  DPRINTS(DBG_GET_TOTAL_SIZE, "numChips: ");
  DPRINTLN(DBG_GET_TOTAL_SIZE, header.numChips);

  return (((unsigned long)header.chipLastAddress + 1L) * header.numChips);
}
unsigned long EEPROM_BankWriter::getFreeSpace() const {
  DASSERT(flags.initialized);
  unsigned long result = 0;
  // free space cannot possibly be calculated if memory is full.
  if (flags.memoryFull != true) {
    result = ((unsigned long) header.chipLastAddress + 1L - header.firstFreeByte + (header.numChips - header.firstFreeChip - 1L) * ((unsigned long)header.chipLastAddress + 1L));
  }
  return result;
}

bool EEPROM_BankWriter::storeOneRecord(const byte* data, const byte dataSize) {
  DBG_TIMER("EEPROM::storeOneRec");
  DASSERT(flags.initialized);
  DASSERT(dataSize == header.recordSize);
  return storeData(data, dataSize);
}

bool EEPROM_BankWriter::storeData(const byte*data, const byte size) {
  DPRINTSLN(DBG_STORE_TO_EEPROM, "storeToEEPROM");
  DASSERT(flags.initialized);

  // First handle simple cases. 
  if (flags.memoryFull == true) return false;
  if (size==0) return true;

  bool result = false;
  unsigned long available = (unsigned long)header.chipLastAddress - header.firstFreeByte + 1;
  if (size <= available) {
    DPRINTSLN(DBG_STORE_TO_EEPROM, "enough space in the current chip");
    byte written = writeToEEPROM(header.firstFreeChip, header.firstFreeByte, data, size);
    if (written == available) {
    header.firstFreeByte = 0;
    header.firstFreeChip ++;
  }
  else {
    header.firstFreeByte += written;
  }
  if (size == written) result = true;
  flags.headerToBeUpdated = true;
  }
else
{
  DPRINTSLN(DBG_STORE_TO_EEPROM, "not enough space in the current chip");
    byte size1 = available;
    byte size2 =  size - size1;
    byte written = writeToEEPROM(header.firstFreeChip, header.firstFreeByte, data, size1);
    if (written == size1) {
      DPRINTSLN(DBG_STORE_TO_EEPROM, "processing part 2");
      if (header.firstFreeChip == header.numChips - 1) {
        DPRINTSLN(DBG_STORE_TO_EEPROM, "no more chip");
        flags.memoryFull = true;
        // Do not set firstFreeChip (there is no first free chip) not firstFreeByte (there is none).
        // Attempting to do something like header.firstFreeByte = header.firstFreeByte + size1; would cause firstFreeByte to overflow with 64k memories.
        flags.headerToBeUpdated = true;
        result = false;
      } else {
        header.firstFreeChip++;
        byte written2 = writeToEEPROM(header.firstFreeChip, 0, &data[written], size2);
        if (written + written2 == size) {
          result = true;
          flags.headerToBeUpdated = true;
        } else {
          DPRINTSLN(DBG_STORE_TO_EEPROM, "Error writing 2nd part");
        }
        header.firstFreeByte = written2;
      }
    }
    else {
      DPRINTSLN(DBG_STORE_TO_EEPROM, "Error writing first part");
    }
  }
  DPRINTS(DBG_STORE_TO_EEPROM_PRINT_HEADER, "first free chip ");
  DPRINTLN( DBG_STORE_TO_EEPROM_PRINT_HEADER, header.firstFreeChip);
  DPRINTS( DBG_STORE_TO_EEPROM_PRINT_HEADER, "first free byte");
  DPRINTLN(DBG_STORE_TO_EEPROM_PRINT_HEADER, header.firstFreeByte);
  return result;

}

bool EEPROM_BankWriter::doIdle(bool forceImmediateMaintenance) {

  if (!forceImmediateMaintenance && (timeElapsedSinceMaintenance < maintenancePeriodInSec * 1000)) return false;

  timeElapsedSinceMaintenance = 0;

  if (flags.headerToBeUpdated == true) {
    byte numBytes = writeToEEPROM(0, 0, (byte*) &header, sizeof header);
    if (numBytes == sizeof header) {
      flags.headerToBeUpdated = false;
      return true;
    }
  }
  return false;
}

void EEPROM_BankWriter::erase() {
  DASSERT(flags.initialized);
  eraseFrom(0, sizeof(EEPROM_Header));
}

void   EEPROM_BankWriter::eraseFrom(byte chipId, EEPROM_Address addressInChip) {
  DASSERT(flags.initialized);
  header.firstFreeChip = chipId;
  header.firstFreeByte = addressInChip;
  flags.headerToBeUpdated = true;
  flags.memoryFull = false;
  doIdle(true);
}

byte EEPROM_BankWriter::readFromEEPROM(const byte EEPROM_SequenceNumber, const EEPROM_Address address, byte * buffer, const byte size) const
{
  DASSERT(EEPROM_SequenceNumber < 8);
  byte I2C_Address = hardware->getExternalEEPROM_I2C_Address(EEPROM_SequenceNumber);
  DPRINTS(DBG_READ_WRITE, "reading: I2C_Address=0x");
  DPRINT(DBG_READ_WRITE, I2C_Address, HEX);
  DPRINTS(DBG_READ_WRITE, ", address=0x");
  DPRINT(DBG_READ_WRITE, address, HEX);
  DPRINTS(DBG_READ_WRITE, ", size=");
  DPRINTLN(DBG_READ_WRITE, size);
  byte read = ExtEEPROM::readData(I2C_Address, address, buffer, size);
  return read;
}

byte EEPROM_BankWriter::writeToEEPROM(const byte EEPROM_SequenceNumber, const unsigned int address, const byte * buffer, const byte size) {
  DASSERT(EEPROM_SequenceNumber < 8);
  byte I2C_Address = hardware->getExternalEEPROM_I2C_Address(EEPROM_SequenceNumber);
  DPRINTS(DBG_READ_WRITE, "writing: I2C_Address=0x");
  DPRINT(DBG_READ_WRITE, I2C_Address, HEX);
  DPRINTS(DBG_READ_WRITE, ", address=0x");
  DPRINT(DBG_READ_WRITE, address, HEX);
  DPRINTS(DBG_READ_WRITE, ", size=");
  DPRINTLN(DBG_READ_WRITE, size);
  byte  written = ExtEEPROM::writeData(I2C_Address, address, buffer, size);
  return written;
}
