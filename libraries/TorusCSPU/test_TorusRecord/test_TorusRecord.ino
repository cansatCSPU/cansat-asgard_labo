#include "TorusRecord.h"
#define DEBUG_CSPU
#define USE_ASSERTIONS
#include "DebugCSPU.h"
#include "StringStream.h"

#define DBG 1

class CansatRecord_Test {
  public:
    void testClear(TorusRecord &rec) {
      DPRINTLN(DBG, "Test: clearing record [BEGIN]");
      
      initValues(rec);
      clearValues(rec);

      checkClearedValues(rec);

      DPRINTLN(DBG, "Test successful");
      DPRINTLN(DBG, "Test: clearing record [END]");
    }
    void testCSVLen(TorusRecord &rec) {
      DPRINTLN(DBG, "Test: CSV length [BEGIN]");
      initValues(rec);

      String s;
      StringStream ss(s);

      rec.printCSV_SecondaryMissionData(ss, true, true);
      DASSERT(rec.getSecondaryMissionMaxCSV_Size() >= s.length());

      s = "";

      rec.printCSV_SecondaryMissionHeader(ss, true, false);
      DASSERT(rec.getSecondaryMissionCSV_HeaderSize() == s.length());
      
      DPRINTLN(DBG, "Test successful");
      DPRINTLN(DBG, "Test: CSV length [END]");
    }
    void testReadWriteBinary(TorusRecord &rec) {
      DPRINTLN(DBG, "Test: read/write binary [BEGIN]");
      
      initValues(rec);
      uint8_t binSize = rec.getBinarySizeSecondaryMissionData();
      byte bin[binSize];
      
      DASSERT(rec.writeBinarySecondaryMissionData(bin, binSize) == binSize);

      clearValues(rec);

      DASSERT(rec.readBinarySecondaryMissionData(bin, binSize) == binSize);
      
      checkValues(rec);

      DPRINTLN(DBG, "Test successful");
      DPRINTLN(DBG, "Test: read/write binary [END]");
    }
    void testCSVPrint(TorusRecord &rec) {
      DPRINTLN(DBG, "Test: CSV print [BEGIN]");

      DPRINTLN(DBG, "Printing secondary mission CSV Header without additional separators")
      rec.printCSV_SecondaryMissionHeader(Serial, false, false);
      DPRINTLN(DBG, "\nPrinting secondary mission CSV Header without preceding separator, but with final separator")
      rec.printCSV_SecondaryMissionHeader(Serial, false, true);
      DPRINTLN(DBG, "\nPrinting secondary mission CSV Header without final separator, but with preceding separator")
      rec.printCSV_SecondaryMissionHeader(Serial, true, false);
      DPRINTLN(DBG, "\nPrinting secondary mission CSV Header with preceding and final separator")
      rec.printCSV_SecondaryMissionHeader(Serial, true, true);

      DPRINTLN(DBG, "\nPrinting secondary mission CSV Data without additional separators")
      rec.printCSV_SecondaryMissionData(Serial, false, false);
      DPRINTLN(DBG, "\nPrinting secondary mission CSV Data without preceding separator, but with final separator")
      rec.printCSV_SecondaryMissionData(Serial, false, true);
      DPRINTLN(DBG, "\nPrinting secondary mission CSV Data without final separator, but with preceding separator")
      rec.printCSV_SecondaryMissionData(Serial, true, false);
      DPRINTLN(DBG, "\nPrinting secondary mission CSV Data with preceding and final separator")
      rec.printCSV_SecondaryMissionData(Serial, true, true);
      
      
      DPRINTLN(DBG, "\nNote: Please check for yourself if the test was successful");
      DPRINTLN(DBG, "Test: CSV print [END]");
    }
    void testHumanReadablePrint(TorusRecord &rec) {
      DPRINTLN(DBG, "Test: Human readable print [BEGIN]");
      rec.printSecondaryMissionData(Serial);
      DPRINTLN(DBG, "Note: Please check for yourself if the test was successful");
      DPRINTLN(DBG, "Test: Human readable print [END]");
    }
    
  private:
    void initValues(TorusRecord &rec) {
      rec.parachuteRopeLen = 12345;
      rec.parachuteTargetRopeLen = 23456;
    }
    void clearValues(TorusRecord &rec) {
      rec.clearSecondaryMissionData();
    }
    void checkValues(TorusRecord &rec) {
      DASSERT(rec.parachuteRopeLen == 12345);
      DASSERT(rec.parachuteTargetRopeLen == 23456);
    }
    void checkClearedValues(TorusRecord &rec) {
      DASSERT(rec.parachuteRopeLen == 0);
      DASSERT(rec.parachuteTargetRopeLen == 0);
    }
} ;

void setup() {
  DINIT(115200);

  TorusRecord rec;
  CansatRecord_Test test;

  test.testClear(rec);
  test.testCSVLen(rec);
  test.testReadWriteBinary(rec);
  test.testCSVPrint(rec);
  test.testHumanReadablePrint(rec);

  DPRINTLN(DBG, "Testing procedure complete");
}

void loop() {
  // put your main code here, to run repeatedly:

}
