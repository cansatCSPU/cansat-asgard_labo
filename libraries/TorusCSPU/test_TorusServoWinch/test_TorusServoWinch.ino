#ifndef ARDUINO_ARCH_SAMD
#error "Incompatible board (SAMD21 processor required)."
#endif

#include "TorusServoWinch.h"
#include "elapsedMillis.h"

#define USE_ASSERTIONS
#define DEBUG_CSPU
#include "DebugCSPU.h"




const byte PWM_Pin = 9;
const uint16_t motorSpeedToUse = 10; // (mm/s)
bool userQuitted = false;
elapsedMillis elapsedSinceTargetChange = 20000;
elapsedMillis readDataCycle = 0;
uint16_t readDataCycleTarget = 500;
uint16_t nextTarget = 1250;

TorusServoWinch servo;
TorusRecord rec;


void waitForUser(char msg[] = "Press any key to continue") {
  while (Serial.available() > 0) {
    Serial.read();
  }
  Serial.println(msg);
  Serial.flush();
  while (Serial.available() == 0) {
    delay(300);
  }
  Serial.read();
}

bool userQuits() {
  if (Serial.available()) return true;
  else return false;
}

void setup() {
  DINIT(115200);

  Serial.println("When motor is powered on, press enter anytime to stop it");
  waitForUser("Press enter to start");

  servo.begin(PWM_Pin, motorSpeedToUse);

}


void loop() {
  delay(10);

  servo.run();

  if (readDataCycle >= readDataCycleTarget) {
    readDataCycle = 0;

    if (userQuitted) return;

    userQuitted = userQuits();
    if (userQuitted) {
      servo.end();
      return;
    }

    if (elapsedSinceTargetChange > 20000) {
      servo.setTarget(ServoWinch::ValueType::pulseWidth, nextTarget);
      nextTarget = (nextTarget == 1750 ? 1250 : 1750);
      elapsedSinceTargetChange = 0;
    }

    servo.readData(rec);
    rec.print(Serial);
  }
}
