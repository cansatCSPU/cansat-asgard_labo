/*
	Specific configuration for the Torus project
*/

#pragma once
#include "ServoWinch.h"

// ********************************************************************
// ************************ Mission parameters ************************
// ********************************************************************

constexpr uint8_t servoWinch_mmPerMove = 2;		// Number of mm to move each time the winch position is updated (once every AsynchServoWinch::updateFrequency)
constexpr byte servoWinchPWM_PinNbr = 2;		// Number of PWM pin used to control the servo winch.

constexpr ServoWinch::ValueType servoInitAndEndPositionType = ServoWinch::ValueType::ropeLength; /**< Type of servoInitAndEndPosition */
constexpr uint16_t servoInitAndEndPosition = 25; /**< Position of the servo to be set at the beginning and at the end. */
constexpr uint16_t servoOvershootWhenRetracting = 20; /**< By how much does the servo have to overshoot when retracting */
constexpr uint16_t servoOvershootWhenExtending = 0;   /**< By how much does the servo have to overshoot when extending */
