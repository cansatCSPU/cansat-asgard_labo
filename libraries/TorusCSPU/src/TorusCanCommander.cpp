/*
 * TorusCanCommander.cpp
 */

#define DEBUG
#include "DebugCSPU.h"
#include "CansatInterface.h"
#include "TorusCanCommander.h"

#define DBG_DIAGNOSTIC 1

TorusCanCommander::TorusCanCommander(unsigned long int theTimeOut) :
		RT_CanCommander(theTimeOut), servo(NULL) {
}

#ifdef RF_ACTIVATE_API_MODE
void TorusCanCommander::begin(CansatXBeeClient &xbeeClient, SdFat* theSd,
		AcquisitionProcess* theProcess, AsyncServoWinch* theServo) {
	RT_CanCommander::begin(xbeeClient, theSd, theProcess);
	servo = theServo;
}
#else
void TorusCanCommander::begin (Stream& theResponseStream, SdFat* theSd, AcquisitionProcess* theProcess, AsyncServoWinch* theServo) {
	RT_CanCommander::begin(theResponseStream,theSd,theProcess);
	servo=theServo;
}
#endif

bool TorusCanCommander::processProjectCommand(CansatCmdRequestType requestType,
		char* cmd) {
	bool result = true;

	switch (requestType) {
	case CansatCmdRequestType::GetWinchPosition:
		processReq_GetWinchPosition(cmd);
		break;

	case CansatCmdRequestType::SetWinchPosition:
		processReq_SetWinchPosition(cmd);
		break;

	default:
		// Do not report error here: this is done by the superclass.
		result = false;
	} // Switch
	return result;
}

void TorusCanCommander::processReq_SetWinchPosition(char*& nextCharAddress) {
	if (!checkServo()) {
		return;
	}
	long int ropeLength;
	if (!getMandatoryParameter(nextCharAddress, ropeLength,
			"Missing rope length")) {
		return;
	}
	if (ropeLength < 0 || ropeLength > 65535) {
		DPRINTSLN(DBG_DIAGNOSTIC, "Invalid rope length");
		RF_OPEN_CMD_RESPONSE(RF_Stream);
		*RF_Stream << (int) CansatCmdResponseType::InvalidRopeLength << ','
				<< ropeLength << ",rope length must be in range 0-65535";
		RF_CLOSE_CMD_RESPONSE(RF_Stream);
		return;
	}

	// Issue the command
	servo->setTarget(ServoWinch::ValueType::ropeLength, ropeLength);
	RF_OPEN_CMD_RESPONSE(RF_Stream);
	*RF_Stream << (int) CansatCmdResponseType::WinchTargetSet << ','
			<< ropeLength << ",target set";
	RF_CLOSE_CMD_RESPONSE(RF_Stream);

	// Wait at most 5 sec for the servo to reach the target.
	elapsedMillis duration;
	while ((!servo->isAtTarget()) && duration < MaxDelayToReachTarget) {
		servo->run();
		delay(200);
	}
	if (servo->isAtTarget()) {
		RF_OPEN_CMD_RESPONSE(RF_Stream);
		*RF_Stream << (int) CansatCmdResponseType::WinchTargetReached << ','
				<< ropeLength << ",target reached";
		RF_CLOSE_CMD_RESPONSE(RF_Stream);
	} else {
		DPRINTSLN(DBG_DIAGNOSTIC, "Target not set after max delay");
		RF_OPEN_CMD_RESPONSE(RF_Stream);
		*RF_Stream << (int) CansatCmdResponseType::WinchTargetNotReached << ','
				<< ropeLength << "," << MaxDelayToReachTarget
				<< ",target not reached after time-out";
		RF_CLOSE_CMD_RESPONSE(RF_Stream);
	}
}

void TorusCanCommander::processReq_GetWinchPosition(char*& nextCharAddress) {
	if (!checkServo()) {
		return;
	}
	RF_OPEN_CMD_RESPONSE(RF_Stream);
	*RF_Stream << (int) CansatCmdResponseType::CurrentWinchPosition << ','
			<< servo->getCurrRopeLen() << "," << servo->isAtTarget()
			<< ",winch position";
	RF_CLOSE_CMD_RESPONSE(RF_Stream);
}

bool TorusCanCommander::checkServo() const {
	if (!servo) {
		RF_OPEN_CMD_RESPONSE(RF_Stream);
		*RF_Stream << (int) CansatCmdResponseType::NoServoConfigured << ','
				<< ", No servo configured";
		RF_CLOSE_CMD_RESPONSE(RF_Stream);
		return false;
	}
	else return true;
}
