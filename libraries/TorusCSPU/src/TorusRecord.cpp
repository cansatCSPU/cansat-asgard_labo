#include "TorusRecord.h"

void TorusRecord::printCSV_SecondaryMissionData(Stream & str, bool startWithSeparator, bool finalSeparator) const {
  if (startWithSeparator) str << separator;
  str << parachuteRopeLen << separator;
  str << parachuteTargetRopeLen;
  if (finalSeparator) str << separator;
}

void TorusRecord::printCSV_SecondaryMissionHeader(Stream &str, bool startWithSeparator, bool finalSeparator) const {
  if (startWithSeparator) str << separator;
  str << "parachuteRopeLen,parachuteTargetRopeLen";
  if (finalSeparator) str << separator;
}

void TorusRecord::clearSecondaryMissionData() {
  parachuteRopeLen = 0;
  parachuteTargetRopeLen = 0;
}

uint16_t TorusRecord::getSecondaryMissionMaxCSV_Size() const {
  return 1 /*,*/ + 5 /*parachuteRopeLen (uint16_t)*/ + 1 /*,*/ + 5 /*parachuteTargetRopeLen (uint16_t)*/ + 1 /*,*/;
}

uint16_t TorusRecord::getSecondaryMissionCSV_HeaderSize() const {
  return 1 /*,*/ + 39 /*parachuteRopeLen,parachuteTargetRopeLen*/;
}

void TorusRecord::printSecondaryMissionData(Stream& str) const {
  str << "parachuteRopeLen: " << parachuteRopeLen << ENDL;
  str << "parachuteTargetRopeLen: " << parachuteTargetRopeLen << ENDL;
}

uint8_t TorusRecord::writeBinarySecondaryMissionData(uint8_t* const destinationBuffer, uint8_t bufferSize) const {
  uint8_t written = 0;
  uint8_t* dst = destinationBuffer;
  uint8_t remaining = bufferSize;
  written += writeBinary(dst, remaining, parachuteRopeLen);
  written += writeBinary(dst, remaining, parachuteTargetRopeLen);

  return written;
}

uint8_t TorusRecord::readBinarySecondaryMissionData(const uint8_t* const sourceBuffer, const uint8_t bufferSize) {
  uint8_t read = 0;
  const uint8_t* src = sourceBuffer;
  uint8_t remaining = bufferSize;
  read += readBinary(src, remaining, parachuteRopeLen);
  read += readBinary(src, remaining, parachuteTargetRopeLen);

  return read;
}

uint8_t TorusRecord::getBinarySizeSecondaryMissionData() const {
  return sizeof(parachuteRopeLen) + sizeof(parachuteTargetRopeLen);
}
