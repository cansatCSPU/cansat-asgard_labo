#include "TorusServoWinch.h"
#include "TorusConfig.h"

#define DEBUG_CSPU
#include "DebugCSPU.h"

#define DBG 1
#define DBG_TORUSSERVOREADDATA 1
#define DBG_DIAGNOSTIC 1

TorusServoWinch::TorusServoWinch() : AsyncServoWinchSW55136MA() {
  // initializing currRopeLen and targetRopeLen in case readData is called before call to begin
  switch (servoInitAndEndPositionType) {
    case ValueType::pulseWidth :
      currPulseWidth = servoInitAndEndPosition;
      currRopeLen = getRopeLenFromPulseWidth(servoInitAndEndPosition);
      break;
    case ValueType::ropeLength :
      currPulseWidth = getPulseWidthFromRopeLen(servoInitAndEndPosition);
      currRopeLen = servoInitAndEndPosition;
      break;
  }
  targetPulseWidth = currPulseWidth;
  targetRopeLen = currRopeLen;
}

bool TorusServoWinch::begin(byte PWM_Pin, uint8_t mmPerMoveToUse) {
  if (running) {
    DPRINTLN(DBG_DIAGNOSTIC, "Can't call TorusServoWinch::begin because servo is already on.");
    return false;
  }
  return AsyncServoWinchSW55136MA::begin(PWM_Pin, servoInitAndEndPositionType, servoInitAndEndPosition, mmPerMoveToUse);
}

bool TorusServoWinch::end() {
  if (!running) {
    DPRINTLN(DBG_DIAGNOSTIC, "Can't call TorusServoWinch::end because the servo is not on.");
    return false;
  }
  return AsyncServoWinchSW55136MA::end(true, servoInitAndEndPositionType, servoInitAndEndPosition);
}

bool TorusServoWinch::setTarget(ValueType type, uint16_t value) {
  switch (type) {
    case ValueType::ropeLength :
      if (value < currRopeLen) setOvershootBy(servoOvershootWhenRetracting);
      else setOvershootBy(servoOvershootWhenExtending);
      break;
    case ValueType::pulseWidth :
      if (value < currPulseWidth) setOvershootBy(servoOvershootWhenRetracting);
      else setOvershootBy(servoOvershootWhenExtending);
      break;
    default :
      DPRINTLN(DBG, "Unexpected ValueType value. Terminating program.");
      DASSERT (false);
      return false;
  }

  return AsyncServoWinchSW55136MA::setTarget(type, value);
}
void TorusServoWinch::readData(TorusRecord & record) {
  record.parachuteRopeLen = currRopeLen;
  record.parachuteTargetRopeLen = targetRopeLen;
}
