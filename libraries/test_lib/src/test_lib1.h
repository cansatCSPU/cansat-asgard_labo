/*
 * Functions provided by source 1.
 */ 
#pragma once
#include "test_lib_config.h"
/* 
 * Just blink the built-in LED twice and send a debug message on the serial line.
 */

int dummyLibFunctionFromSrc1();

