/*
 * CansatRecordExample.cpp
 */

#include "CansatRecordExample.h"

void CansatRecordExample::initValues() {
		timestamp=654321;
		newGPS_Measures=true;
		GPS_LatitudeDegrees=-76.543211;
		GPS_LongitudeDegrees=167.897654;
		GPS_Altitude=789.2;
#ifdef INCLUDE_GPS_VELOCITY
     GPS_VelocityKnots=123.45;      	/**< velocity over ground in Knots (1 knot = 0.5144447 m/s) */
     GPS_VelocityAngleDegrees=-45.67; /**< Direction of velocity in decimal degrees, 0 = North */
#endif
		temperatureBMP=-1.2;
		pressure=1011.4;
		altitude=1234.6;
#ifdef INCLUDE_DESCENT_VELOCITY
		descentVelocity=243.2341;
#endif
		temperatureThermistor1=4.4;
#ifdef INCLUDE_THERMISTOR2
		temperatureThermistor2=25.5;
#endif
#ifdef INCLUDE_THERMISTOR3
		temperatureThermistor3=12.3;
#endif
		integerData=123;
		floatData=1234.5;
	}

template<class T>
static bool checkFloat(T actual, T expected, T accuracy=0.00001) {
	if (fabs(actual - expected) < accuracy){
		return true;
	}
	else {
		Serial << " *** ERROR: float does not match (accuracy=";
		Serial.print(accuracy, 8);
		Serial << "). Got ";
		Serial.print(actual,8);
		Serial << " instead of ";
		Serial.print(expected, 8);
		Serial << ENDL;
		return false;
	}
}

bool CansatRecordExample::checkValues(bool ignoreTimestamp) {
	uint8_t errors = 0;
	if ((!ignoreTimestamp) && (timestamp != 654321)) {
		errors++;
		Serial << " *** Error in timestamp" << ENDL;
	}
	if (newGPS_Measures != true) {
		errors++;
		Serial << " *** Error in newGPS_Measures" << ENDL;
	}
	if (!checkFloat(GPS_Altitude,789.2f, 0.01f)) {
		errors++;
		Serial << " *** Error in GPS_Altitude" << ENDL;
	}
	if (!checkFloat(GPS_LatitudeDegrees , -76.543211, 1.0E-5)) {
		errors++;
		Serial << " *** Error in GPS_LatitudeDegrees" << ENDL;
	}
	if (!checkFloat(GPS_LongitudeDegrees, 167.897654, 1E-5)) {
		errors++;
		Serial << " *** Error in GPS_LongitudeDegrees" << ENDL;
	}
#ifdef INCLUDE_GPS_VELOCITY
	if (!checkFloat(GPS_VelocityKnots , 123.45, 0.01)) {
		errors++;
		Serial << " *** Error in GPS_VelocityKnots" << ENDL;
	}
	if (!checkFloat(GPS_VelocityAngleDegrees , -45.67, 0.01)) {
		errors++;
		Serial << " *** Error in GPS_VelocityAngleDegrees" << ENDL;
	}
#endif

	if (!checkFloat(altitude , 1234.6f, 0.1f)) {
		errors++;
		Serial << " *** Error in altitude" << ENDL;
	}
	if (!checkFloat(pressure , 1011.4f, 0.1f)) {
		errors++;
		Serial << " *** Error in pressure" << ENDL;
	}
#ifdef INCLUDE_DESCENT_VELOCITY
	if (!checkFloat(descentVelocity , 243.23f, 0.01f)) {
		errors++;
		Serial << " *** Error in descentVelocity" << ENDL;
	}
#endif

	if (!checkFloat(temperatureBMP , -1.2f, 0.1f)) {
		errors++;
		Serial << " *** Error in temperatureBMP" << ENDL;
	}
	if (!checkFloat(temperatureThermistor1 , 4.4f, 0.1f)) {
		errors++;
		Serial << " *** Error in temperatureThermistor1" << ENDL;
	}
#ifdef INCLUDE_THERMISTOR2
	if (!checkFloat(temperatureThermistor2 , 25.5f, 0.1f)) {
		errors++;
		Serial << " *** Error in temperatureThermistor2" << ENDL;
	}
#endif
#ifdef INCLUDE_THERMISTOR3
	if (!checkFloat(temperatureThermistor3 , 12.3f, 0.1f)) {
		errors++;
		Serial << " *** Error in temperatureThermistor3" << ENDL;
	}
#endif
	if (integerData != 123) {
		errors++;
		Serial << " *** Error in integerData (got "<< integerData << ", expected " << 123 << ")" << ENDL;
	}
	if (!checkFloat(floatData,1234.5f, 0.1f)) {
		errors++;
		Serial << " *** Error in floatData" << ENDL;
	}
	if (errors > 0) {
		Serial << "Record content" << ENDL;
		print(Serial);
	}
	return (errors == 0);
}

void CansatRecordExample::printCSV_SecondaryMissionData(Stream & str,
		bool startWithSeparator, bool finalSeparator) const {
	if (startWithSeparator)
		str << separator;
	str << integerData << separator;
	printCSV(str, floatData);
	if (finalSeparator)
		str << separator;
}

void CansatRecordExample::printCSV_SecondaryMissionHeader(Stream & str,
		bool startWithSeparator, bool finalSeparator) const {
	if (startWithSeparator)
		str << separator;
	str << "integerData,floatData";
	if (finalSeparator)
		str << separator;
}

void CansatRecordExample::clearSecondaryMissionData() {
	integerData = 0;
	floatData = 0.0;
}

uint16_t CansatRecordExample::getSecondaryMissionMaxCSV_Size() const {
	return 2 + 5 + CansatRecord::NumDecimalPositions_Default;
}

uint16_t CansatRecordExample::getSecondaryMissionCSV_HeaderSize() const {
	return 22;
}

void CansatRecordExample::printSecondaryMissionData(Stream& str) const {
	str << "integerData: " << integerData << ENDL;
	str << "floatData  : " << floatData << ENDL;
}

uint8_t CansatRecordExample::writeBinarySecondaryMissionData(
		uint8_t* const destinationBuffer, uint8_t bufferSize) const {
	uint8_t written = 0;
	uint8_t* dst = destinationBuffer;
	uint8_t remaining = bufferSize;
	written += writeBinary(dst, remaining, integerData);
	written += writeBinary(dst, remaining, (uint32_t) scaleFloat(floatData, 10.0f));
	// Store the float with 0.1 accuracy in range 0 .. 6553,5
	return written;
}

uint8_t CansatRecordExample::getBinarySizeSecondaryMissionData() const {
	return sizeof(integerData) + sizeof(uint32_t);
}

uint8_t CansatRecordExample::readBinarySecondaryMissionData(
		const uint8_t* const sourceBuffer, const uint8_t bufferSize) {
	uint8_t read = 0;
	const uint8_t* src = sourceBuffer;
	uint8_t remaining = bufferSize;
	uint32_t tmp_uint32;
	read +=readBinary(src, remaining, integerData);
	read +=readBinary(src, remaining, tmp_uint32, floatData, 10.0f);

	return read;
}
