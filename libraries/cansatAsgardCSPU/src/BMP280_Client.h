/*
    BMP280_Client.h
*/
#pragma once

#include "Adafruit_BMP280.h"
#include "Adafruit_Sensor.h"
#include "Wire.h"
#include "CansatRecord.h"

/** @ingroup cansatAsgardCSPU
    @brief Interface class to the BMP280 sensor (connected to the I2C bus),
    providing the required methods to access individual
    data and to fill the relevant fields of a CanSat record.
*/
class BMP280_Client {
  public:
    BMP280_Client();
    /**
        @brief Initialize the BMP280 sensor before use.
        @param theSeaLevelPressure The pressure at sea level in hPa (required
        	   to compute altitude).
        @return True if initialization was successful, false otherwise.
    */
    bool begin(float theSeaLevelPressure);

    /** Obtain the atmospheric pressure in hPa
     *  @return The current pressure.*/
    float getPressure();

    /** Obtain the altitude (from sea level) in m as computed using the current pressure and the
     *  sea level pressure provide durin initialization.
     *  @return The current altitude. 0 if the calculation fails for any reason.*/
    float getAltitude();

    /** Obtain the atmospheric temperature in °C
     *  @return The current temperature.*/
    float getTemperature() ;

    /** @brief Read sensor data and populate data record.
	 *  @param record The data record to populate (fields BMPtemperature, altitude and pressure).
	 *  @return True if reading was successful.
	 */
     bool readData(CansatRecord& record);

  protected:
    Adafruit_BMP280 bmp;	//**< The driver class.
    float seaLevelPressure; //**< Atmospheric pressure at sea level, in hPA
};
