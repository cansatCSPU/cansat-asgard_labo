/*
   ThermistorVMA320.h
*/

#pragma  once
#include "ThermistorSteinhartHart.h"

/**  @ingroup cansatAsgardCSPU
 *  @brief This a subclass of thermistorClient customized for thermistor VMA320.
 *  When looking from components side, thermistor facing up:
 *        - middle point is left
 *        - VCC is in the middle
 *        - Gnd is at the right.
 *        Use this class to read thermistor resistance and convert to degrees.
 */
class ThermistorVMA320: public ThermistorSteinhartHart {
  public:
  /**
   *  @param theVcc  The voltage supplied to the serial resistor+thermistor assembly
   *  @param theAnalogPinNbr This analog pin connected to the middle-point.
   *  @param theSerialResistor The serial resistor (ohms).
   */
    ThermistorVMA320(float theVcc, byte theAnalogPinNbr, float theSerialResistor ):
      ThermistorSteinhartHart(theVcc, theAnalogPinNbr, 1.0, 1.009249522e-03,  2.378405444e-04, 0.0, 2.019202697e-07, theSerialResistor) {};
};
