#pragma once
#include "AsyncServoWinch.h"

/** @ingroup cansatAsgardCSPU
    @brief Subclass of AsyncServoWinch that is specialised for the SW5513-6MA servo winch.
*/
class AsyncServoWinchSW55136MA : public AsyncServoWinch {
  public:
    AsyncServoWinchSW55136MA();

    uint16_t getRopeLenFromPulseWidth(uint16_t pulseWidth) const override;

    uint16_t getPulseWidthFromRopeLen(uint16_t ropeLen) const override;


  private:
    /** Get the size of the SW55136MA_RopeLenToPulseWidth conversion table
      @return the size of the conversion table
    */
    static uint8_t getConversionTableSize();

    static constexpr uint16_t SW55136MA_MaxRopeLen = 500;		/**< The rope length when fully extended for the SW5513-6MA servo winch. (mm) */
    static constexpr uint16_t SW55136MA_MinPulseWidth = 1000;	/**< The minimum pulse width for the SW5513-6MA servo winch.*/
    static constexpr uint16_t SW55136MA_MaxPulseWidth = 2000;	/**< The maximum pulse width for the SW5513-6MA servo winch.*/

    static constexpr uint16_t SW55136MA_RopeLenToPulseWidth[] = {1000, 1013, 1035, 1061, 1081, 1098, 1120, 1145, 1170, 1193, 1227, 1257, 1286, 1301, 1317}; /**< The conversion table rope length -> pulse width. SW55136MA_RopeLenToPulseWidth[i] is the pulse width when the rope is extended by 1 cm */

    friend class ServoWinch_Test;	/**< Friend class used for testing */
};
