/*
 * Test for class BMP280_Client.
 *
 * Wiring:
 *    µC		BMP280
 *    3V		Vin
 *    GND		GND
 *    SCL		SCK (with pull-up resistor to Vin)
 *	  SDA		SDI (with pull-up resistor to Vin)
 */

#include "elapsedMillis.h"
#include "BMP280_Client.h"

CansatRecord record;
constexpr float mySeaLevelPressure = 1011.0;
constexpr bool  csvFormat = false;

BMP280_Client bmp;

void setup() {
  // put your setup code here, to run once:
  DINIT(115200);
  Wire.begin();
  if (!bmp.begin(mySeaLevelPressure)) {
    Serial.print("Could not find a valid BMP280 sensor, check wiring!");
    exit(-1);
  }
}

void loop() {
  bool result = bmp.readData(record);
  if (result == true) {
    if (csvFormat) {
      Serial << millis() << "," << record.temperatureBMP << "," << record.pressure << "," << record.altitude << ENDL;

    } else {
      Serial << millis() << ": temp: " << record.temperatureBMP << "°C, pressure:" << record.pressure << " hPa, " << "altitude:" << record.altitude << " m" << ENDL;
    }
  }
  delay(1000);
}
