/*
   Main file of the final on-board software for CanSat IsaTwo project.
   Version integrating the RT_Commander.
   CSPU, 2018-19
*/

#define DEBUG_CSPU
#include "DebugCSPU.h"
#define DBG_RECEPTION 0

#include "IsaTwoConfig.h"
#include "IsaTwoAcquisitionProcess.h"
#include "RT_Commander.h"
#include "IsaTwoInterface.h"
#ifdef RF_ACTIVATE_API_MODE
#include "IsaTwoXBeeClient.h"
#else
#include "LineStream.h"
#endif

IsaTwoAcquisitionProcess process;
RT_Commander commander((byte) IsaTwoRecordType::CmdRequest, RT_CommanderTimeoutInMsec);
#ifdef RF_ACTIVATE_API_MODE
IsaTwoXBeeClient* rf = NULL;
char msg[RF_LongestStringLength];
#else
LineStream lineRF_Stream;
Stream* rf = NULL;
#endif

#ifdef INIT_SERIAL
void printWelcomeBoard() {
  Serial << "============================================" << ENDL;
  Serial << "=        Master Controller software        =" << ENDL;
  Serial << "============================================" << ENDL << ENDL;
  Serial << "  #     # " << ENDL;
  Serial << "  ##   ##    ##     ####   #####  ######  #####"  << ENDL;
  Serial << "  # # # #   #  #   #         #    #       #    #" << ENDL;
  Serial << "  #  #  #  #    #   ####     #    #####   #    #" << ENDL;
  Serial << "  #     #  ######       #    #    #       #####"  << ENDL;
  Serial << "  #     #  #    #  #    #    #    #       #   #"  << ENDL;
  Serial << "  #     #  #    #   ####     #    ######  #    #" << ENDL << ENDL;
#ifndef ARDUINO_SAMD_FEATHER_M0_EXPRESS
  Serial << "*** Warning: Not running on Feather M0 Express board ?? ? ***" << ENDL << ENDL;
#endif
  Serial << "Hardware pins : " << ENDL;
  Serial << "  Serial debug (active LOW)     : " << DbgCtrl_MasterDigitalPinNbr
         << " (" << digitalRead(DbgCtrl_ImagerDigitalPinNbr) << ")" << ENDL;
  Serial << "  CO2 Sensor analog wake        : " << CO2_AnalogWakePinNbr << ENDL;
  Serial << "  Thermistor analog in          : " << Thermistor_AnalogInPinNbr << ENDL;
  Serial << "  Imager Ctrl line (active LOW) : " << ImagerCtrl_MasterDigitalPinNbr << ENDL;
  Serial << "  Master SD Card chip - select  : " <<  MasterSD_CardChipSelect <<  ENDL;

#ifdef USE_NXP_PRECISION_IMU
  Serial << "Using NXP Precision IMU board, with reference : '" << IMU_REFERENCE << "'" << ENDL;
#else
  Serial << "Using LSM9DS0 IMU board, with reference : '" << IMU_REFERENCE << "'" << ENDL;
#endif
  Serial << "RT-Commander request code = " << (byte) IsaTwoRecordType::CmdRequest << ENDL;
  Serial << "Acquisition period: " << IsaTwoAcquisitionPeriod << " msec " << ENDL;
  Serial << ENDL;
} // PrintWelcomeBoard
#endif

void setup() {
#ifdef INIT_SERIAL
  DINIT_IF_PIN(USB_SerialBaudRate, DbgCtrl_MasterDigitalPinNbr);
  printWelcomeBoard();
#endif
  process.init();
  DPRINTSLN(DBG_SETUP, "AcquisitionProcess initialized");

  IsaTwoHW_Scanner* hw = process.getHardwareScanner();
#ifdef RF_ACTIVATE_API_MODE
  rf = hw->getRF_XBee();
#else
  rf = hw->getRF_SerialObject();
#endif

  // if the RF interface is available, we use the RT-Commander,
  // otherwise, just run the AcquisitionProcess
  if (rf) {
#ifndef RF_ACTIVATE_API_MODE
    lineRF_Stream.begin(*rf, maxUplinkMsgLenght);
#endif
    commander.begin(*rf, process.getSdFat() , &process);
    DPRINTSLN(DBG_SETUP, "RT - Commander initialized");
  } else {
    DPRINTSLN(DBG_SETUP, "RT - Commander NOT IN USE");
  }

  DPRINTSLN(DBG_SETUP, "Setup complete");
}

void loop() {
  DPRINTSLN(DBG_LOOP, "*** Entering processing loop");
  if (rf) {
#ifdef RF_ACTIVATE_API_MODE
    uint8_t* payloadPtr;
    uint8_t payloadSize;
    if (rf->receive(payloadPtr, payloadSize)) {
      DPRINTSLN(DBG_RECEPTION, "Received packet");
      if (payloadSize > RF_LongestStringLength) {
        DPRINTSLN(DBG_DIAGNOSTIC, "Received string message too large to fit in buffer. Ignored");
      } else  if (payloadSize == 0) {
        DPRINTSLN(DBG_DIAGNOSTIC, "*** Received null-size payload");
      } else {
        // Process incoming message.
        switch (payloadPtr[0]) {
          case (uint8_t) IsaTwoRecordType::CmdResponse:
            DPRINTSLN(DBG_DIAGNOSTIC, "*** Received a Cmd Response ?? Ignored.");
            break;
          case (uint8_t) IsaTwoRecordType::StatusMsg:
            DPRINTSLN(DBG_DIAGNOSTIC, "*** Received a Status msg ?? Ignored.");
            break;
          case (uint8_t) IsaTwoRecordType::DataRecord:
            DPRINTSLN(DBG_DIAGNOSTIC, "*** Received a Data Record ?? Ignored.");
            break;
          case (uint8_t) IsaTwoRecordType::CmdRequest:
            if (!rf->getString(msg, payloadPtr, payloadSize)) {
              DPRINTSLN(DBG_DIAGNOSTIC, "*** Error: could not extract string from payload. (ignored)");
            }
            break;
          default:
            DPRINTS(DBG_DIAGNOSTIC, "*** Error: unexpected RecordType received (ignored):");
            DPRINTLN(DBG_DIAGNOSTIC, payloadPtr[0]);
        } // switch
      } // anything valid received.
    } // anything received
#else
    const char* msg = lineRF_Stream.readLine();
#endif
  if ((msg != nullptr)  && (*msg != '\0')) {
    DPRINTS(DBG_LOOP_MSG, "Received '");
    DPRINT(DBG_LOOP_MSG, msg);
    DPRINTSLN(DBG_LOOP_MSG, "'");
    commander.processMsg(msg);
#ifdef RF_ACTIVATE_API_MODE
    *msg='\0'; // reset msg buffer to avoid processing it again.
#endif
  }
  if (commander.currentState() == RT_Commander::State_t::Acquisition) {
    process.run();
  }
} else  // rf not available.
{
  process.run();
}
DDELAY(DBG_LOOP, 500);  // Slow main loop down for debugging.
DPRINTSLN(DBG_LOOP, "*** Exiting processing loop");
}
