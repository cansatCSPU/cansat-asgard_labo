#define DEBUG
#include "DebugCSPU.h"

void setup() {
  // put your setup code here, to run once:
  DINIT(115200)

  Serial << "testing floats: " << ENDL;
  Serial << "-76.543211 : ";
  Serial.println(-76.543211f, 10);
  Serial << "-76.543212 : ";
  Serial.println(-76.543212f, 10);

 Serial << "-76.54321 : ";
  Serial.println(-76.543211f, 10);
  Serial << "-76.54322 : ";
  Serial.println(-76.543212f, 10);

   Serial << "-76.543211 *100f: ";
  Serial.println(-76.543211f*10000.0f, 10);
  Serial << "-76.543212*100f : ";
  Serial.println(-76.543212f*10000.0f, 10);

}

void loop() {
  // put your main code here, to run repeatedly:

}
